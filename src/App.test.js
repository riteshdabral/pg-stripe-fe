import {shallow} from 'enzyme'
import App from './App';

let wrapper = shallow(<App/>);

describe('ROOT COMPONENT TESTING', ()=>{

  it('Check if a class .App exists', () => {
    expect(wrapper.find('.App')).toBeDefined();
  });

  it('Should have a child', () => {
    expect(wrapper.find('.App').children()).toHaveLength(1);
  });

})
