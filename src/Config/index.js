 
 // google client id
 export const GoogleClientId  = process.env.REACT_APP_GOOGLE_CLIENT;

 // facebook client id
 export const FacebookAppId  = process.env.REACT_APP_FACEBOOK_APPID;

 // stripe api key
 export const StripeTestApiKey = process.env.REACT_APP_STRIPE_TEST_API_KEY;
