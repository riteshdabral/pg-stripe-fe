
import {shallow} from 'enzyme'
import GoogleLoginBtn from './GoogleLoginBtn'

let wrapper = shallow(<GoogleLoginBtn/>);

describe('GOOGLE LOGIN BTN COMPONENT', ()=>{
    
    it(`Check if element with id='google-btn-div' exists`, ()=>{
        expect(wrapper.find('#google-btn-div')).toBeDefined();
    })

    it(`Check presence of only one children`, ()=>{
        expect(wrapper.find('#google-btn-div').children()).toHaveLength(1);
    })

})