
 import React from 'react';
 import {GoogleLogin} from 'react-google-login';
 import {GoogleClientId} from '../../Config/index';



function GoogleLoginBtn({setLocalStorage,setChildErrors}) {

    /**
     * 
     * @param {*} res : response received from google when successfully
     * sign up/in
     */
    const successResponseGoogle = async(res)=>{
        const {email,name} = res.profileObj;
        setLocalStorage({email,name});
    }


    /**
     * 
     * @param {*} res : response received from google when sign up/in
     * unsuccessful
     * 
     */
    const failureResponseGoogle = async(err)=>{
        setChildErrors(`Google Error : ${err.error}`)
    }

    return (
        <div id="google-btn-div">
            <GoogleLogin 
                clientId={GoogleClientId}
                onSuccess={successResponseGoogle}
                onFailure={failureResponseGoogle}
                cookiePolicy={'single_host_origin'}
                render={renderProps => (
                    <button onClick={renderProps.onClick} disabled={renderProps.disabled}
                    style={styles.googleBtn}
                    >
                        <i className="fab fa-google"></i> Login with google
                    </button>
                )}
            />
        </div>
    )
}

export default GoogleLoginBtn

const styles = {
    googleBtn:{
        fontFamily: "Helvetica,sans-serif",
        fontWeight: "700",
        WebkitFontSmoothing: "antialiased",
        color: "#fff",
        cursor: "pointer",
        display: "inline-block",
        fontSize: "calc(.27548vw + 12.71074px)",
        textDecoration: "none",
        textTransform: "uppercase",
        transition: "background-color .3s,border-color .3s",
        backgroundColor: "#4c69ba",
        border: "calc(.06887vw + .67769px) solid #4c69ba",
        padding: "calc(.34435vw + 13.38843px) calc(.34435vw + 18.38843px)",
    }
}
