 import React from 'react';
 import FacebookLogin from 'react-facebook-login';
 import {FacebookAppId} from '../../Config/index'

 
function FBLoginBtn({setLocalStorage,setChildErrors}) {

    /**
     * 
     * @param {*} res : response from FaceBook
     */
    const responseFacebook = async(res) => {
        if(res.status==='unknown'){
            setChildErrors(`Facebook: ${res.status}`);
        }
        else{
            const {email,name} = res;
            setLocalStorage({email,name});
        }
    }

    return (
        <div id="facebook-btn-div">
            <FacebookLogin 
                appId={FacebookAppId}
                fields="name,email,picture"
                callback={responseFacebook} 
                icon="fa-facebook"           
            />
        </div>
    )
}

export default FBLoginBtn
