
import {shallow} from 'enzyme'
import FacebookLoginBtn from './FBLoginBtn'

let wrapper = shallow(<FacebookLoginBtn/>);

describe('FACEBOOK LOGIN BTN COMPONENT', ()=>{
    
    it(`Check if element with id='facebook-btn-div' exists`, ()=>{
        expect(wrapper.find('#facebook-btn-div')).toBeDefined();
    })

    it(`Check presence of only one children`, ()=>{
        expect(wrapper.find('#facebook-btn-div').children()).toHaveLength(1);
    })

})