/**
 * Card element testing 
 * 
 */

 import {shallow} from 'enzyme'
 import CardInput from './Card'

 let wrapper = shallow(<CardInput/>)

 describe('CARD ELEMENT', ()=>{
     it('Have no child', ()=>{
         expect(wrapper.children()).toHaveLength(0);
     })
 })

