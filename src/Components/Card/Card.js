import React from 'react'
import {CardElement} from '@stripe/react-stripe-js';


export default function CardInput() {
    return (
        <CardElement
            options={{
                style: {
                    base: {
                        'color': '#32325d',
                        'fontFamily': '"Helvetica Neue", Helvetica, sans-serif',
                        'fontSmoothing': 'antialiased',
                        'fontSize': '16px',
                        '::placeholder': {
                          color: '#aab7c4',
                        },
                    },
                    invalid: {
                        color: '#9e2146',
                    },
                },
            }}
        />
    )
}
