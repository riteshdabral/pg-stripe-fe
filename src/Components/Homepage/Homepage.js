 import React,{useState,useEffect} from 'react'
 import {useHistory} from 'react-router-dom'
 
 /* ADDITIONAL COMPONENTS */

 import GoogleLoginBtn from '../GoogleLogin/GoogleLoginBtn'
 import FBLoginBtn from '../FacebookLogin/FBLoginBtn' 
 
 import {Jumbotron,Container,Col,Image,Row,Alert} from 'react-bootstrap'
 import LoginLogo from '../../Assets/Images/Login.jpg'

 import './homepage.css'

function Homepage() {

    const history = useHistory();
    const [alert,setalert] = useState({msg:'',variant:'light'});
    const [show,setShow]   = useState(false);

    const setLocalStorage = (userObj)=>{
        localStorage.setItem('PGuser',JSON.stringify(userObj));
        history.push('/dashboard');
    }

    const setChildErrors = (msg)=>{
        setShow(true);
        setalert({
            "msg":msg,
            "variant":'danger'
        })
    }

    useEffect(()=>{

        // get user from localstorage
        const user = JSON.parse(localStorage.getItem('PGuser'));

        if(user){
            history.push('/dashboard');
        }

    });

    return (
        <Jumbotron style={{padding:"2rem 0"}}  data-test="homepage-jumbotron">

            <Container style={{padding:"10px"}}>
                <Alert variant="success" style={{marginBottom:"10px"}}>Sign in/up using below methods</Alert>

                <Alert 
                    variant={alert.variant} 
                    onClose={() => setShow(false)} 
                    show={show}  
                    className="custom-alert"
                    dismissible
                >
                    {
                        alert.msg ? (alert.msg):('')
                    }
                </Alert>

                <Row>
                    <Col md={6}>
                        <div>
                            <Image src={LoginLogo} style={{width:"100%",height:"100%"}} rounded fluid/>
                            <a href='https://www.freepik.com/vectors/website' style={{marginTop:"0",marginBottom:"5px"}}>Website vector created by stories - www.freepik.com</a>
                        </div>
                    </Col>
                
                    <Col md={6} style={{margin: "50px auto"}}>
                        <h3>Welcome, </h3>
                        <GoogleLoginBtn setLocalStorage={setLocalStorage} setChildErrors={setChildErrors}/>
                        <FBLoginBtn  setLocalStorage={setLocalStorage} setChildErrors={setChildErrors}/>          
                    </Col>
                </Row>
            </Container>
        </Jumbotron>
    )
}

export default Homepage
