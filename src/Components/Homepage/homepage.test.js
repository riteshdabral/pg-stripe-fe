
import {shallow} from 'enzyme'
import Homepage from './Homepage'

let wrapper = shallow(<Homepage/>);

describe('HOMEPAGE COMPONENT', ()=>{
    
    it(`Check if element Jumbotron exists`, ()=>{
        expect(wrapper.find(`[data-test="homepage-jumbotron"]`)).toBeDefined();
    })

    it(`Check presence of only one child (container)`, ()=>{
        expect(wrapper.find(`[data-test="homepage-jumbotron"]`).children()).toHaveLength(1);
    })
})