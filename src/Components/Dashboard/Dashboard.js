/* UTILITY MODULES */
 import React,{useState,useEffect} from 'react'
 import {Button,Alert,Jumbotron,Col,Row,Card,InputGroup,FormControl,Spinner} from 'react-bootstrap'
 import {useStripe, useElements, CardElement} from '@stripe/react-stripe-js';
 import {isEmail} from 'validator'
 import {useHistory} from 'react-router-dom'
 import axios from 'axios'

/* CSS */
 import './dashboard.css'

/* ADDITIONAL COMPONENTS */
 import card_details from './cardDetails'
 import CardInput from '../Card/Card'
 import AmountCard from './AmountCard'
 import LogoutBtn from './LogoutBtn'
 import Rules from './Rules'


 function Dashboard() {

    const stripe = useStripe();
    const elements = useElements();
    const history = useHistory();
    // use state
    const [amount,setamount]   = useState(25);
    const [alert,setAlert]     = useState({msg:'',variant:'light'});
    const [show,setShow]       = useState(false);
    const [loading,setloading] = useState(false);
    const [email,setemail]     = useState('');
    const [name,setname]       = useState('ABC');

    /**
     * Handle the payment submission
     */
    const handlePaymentSubmission = async()=>{

        disableBtn(true);

        if (!stripe || !elements) {
            // Stripe.js has not loaded yet. Make sure to disable
            // form submission until Stripe.js has loaded.

            setShow(true);
            setAlert({
                msg:'Stripe has not loaded yet',
                variant:'warning'
            })

            disableBtn(false);
            return;
        }

        else if(!email || !(isEmail(email))){

            setShow(true);
            setAlert({
                msg:'A valid Email is required',
                variant:'warning'
            })

            disableBtn(false);
            return;
        }

        else {
            let isValidAmt = false;
            for(let i=0;i<card_details.length;i++){
                if(parseInt(card_details[i].amount)===parseInt(amount)){
                    isValidAmt = true;
                    break;
                }
            }
            
            if(!isValidAmt){
                setShow(true);
                setAlert({
                    msg:'Not a valid amount',
                    variant:'warning'
                })
    
                disableBtn(false);
                return;
            }
        }
        //let email = 'test@email.com';
        const url = `${process.env.REACT_APP_SERVER_URL}/pay`;

        // post the request
        axios.post(url,{email,amount:parseInt(amount)})
            .then( async (res)=>{

                const client_secret = res.data['clientSecret'];

                const payment_result = await stripe.confirmCardPayment(client_secret,{
                    payment_method:{
                        card:elements.getElement(CardElement),
                        billing_details:{
                            email:email
                        }

                    }
                });

                if(payment_result.error){
                    setShow(true);
                    setAlert({
                        msg:payment_result.error.message,
                        variant:'danger'
                    })
                }else{
                    if(payment_result.paymentIntent.status === 'succeeded'){
                        setShow(true);
                        setAlert({
                            msg:`Payment of INR ${amount}.00 was successful`,
                            variant:'success'
                        })
                    }
                }
                disableBtn(false);
                
            })
            .catch(err=>{
                setShow(true);
                
                const errMsg = err.response ? (err.response.data.message) : ('Unknown Error Occured');
                 setAlert({
                    msg:errMsg,
                    variant:'danger'
                })
                disableBtn(false);

            })

    }

    /**
     * Disables or enables pay button and loading spinner
     * 
     * @param {*} status : disable or enable 'Pay' button
     */
    const disableBtn = (status=false)=>{
        if(status){
            document.getElementById('pay-btn').disabled = true;
            setloading(true);
            return;
        }

        setloading(false);
        document.getElementById('pay-btn').disabled = false;
    }

    /**
     * Use effect
     */
    useEffect(()=>{
        const user = JSON.parse(localStorage.getItem('PGuser'));

        if(!user){
            history.push('/')
        }
        else{
            setname(user.name);
        }

    },[amount,history]);



     return (
         <Jumbotron data-test="dashboard-jumbotron">         

            <Alert variant="info" style={{fontFamily:"Poppins",fontWeight:"800"}}>
                <span>
                    <p>
                        Logged in as : {name}
                    </p>
                    <LogoutBtn />
                </span>
            </Alert>

             {/* Show different card and amounts */}
            <Row>
                    {
                        card_details ? (card_details.map((eachCard,index)=>
                            <Col key={index} sm={6} md={3}>
                                <AmountCard 
                                    title={eachCard.title}
                                    desc={eachCard.desc}
                                    imgSrc={eachCard.imgSrc}
                                    amount={eachCard.amount}
                                    setAmount={setamount}
                                />
                            </Col>
                        )):(<span>Loading card...</span>)
                    }
            </Row>
            
            
            {/* Success or error alerts */}
            <Alert variant={alert.variant} onClose={() => setShow(false)} show={show} dismissible className="custom-alert">
                {
                    alert.msg ? (alert.msg):('')
                }
            </Alert>

            {/* Checkout card */}
            <Card style={{ width: '500px', margin:"1rem auto", fontFamily:"Poppins",fontWeight:"800"}} id="card-input">
                <Card.Body>
                    <Card.Title style={{fontFamily:"Poppins",fontWeight:"800"}}><Alert variant="info">Checkout ,</Alert></Card.Title>
                    <Card.Body><p id='amount-payable'>Amount payable : {amount}.00 INR</p></Card.Body>
                    <InputGroup style={{marginBottom:"30px"}}>
                        <FormControl
                            placeholder="Billing Email*"
                            aria-label="Email"
                            aria-describedby="basic-addon1"
                            type="email"
                            onChange = {(e)=>{setemail(e.target.value)}}
                        />
                    </InputGroup>

                    <CardInput />

                    <Button variant="success" id="pay-btn" onClick={handlePaymentSubmission} disabled={!stripe} style={{marginTop:"30px"}}>
                        {
                            loading ? (
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                            ) : ('Pay')
                        }
                    </Button>
                </Card.Body>
            </Card>

            <Rules/>

         </Jumbotron>
     )
 }
 
 export default Dashboard
 
