 import React from 'react'
 import {useHistory} from 'react-router-dom'

function LogoutBtn() {

    const history = useHistory();

    /**
     * Clear localstorage, get out
     */
    const handleLogout = ()=>{
        localStorage.removeItem('PGuser');
        history.push('/');
    }

    return (
        <div id="logout-btn" onClick={handleLogout} style={{cursor:"pointer",color:"#E91E63"}}>
            <i className="fas fa-power-off"></i>
        </div>
    )
}

export default LogoutBtn
