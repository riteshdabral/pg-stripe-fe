
 import EducationImg from '../../Assets/Images/education.jpg'
 import FarmerImg from '../../Assets/Images/farmer.jpg'
 import FloodImg from '../../Assets/Images/flood.jpg'
 import HungerImg from '../../Assets/Images/hunger.jpg'

const cardDetails =  [
    {
        id:"education-card",
        title:"Education For All", 
        desc:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        imgSrc:EducationImg,
        amount:"100"
    },
    {
        id:"farmers-relief-card",
        title:"Farmers Camp", 
        desc:"Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
        imgSrc:FarmerImg,
        amount:"250"
    },
    {
        id:"kedarnath-relief-card",
        title:"Kedarnath Flood Relief", 
        desc:"Contrary to popular belief, Lorem Ipsum is not simply random text",
        imgSrc:FloodImg,
        amount:"500"
    },
    {
        id:"hunger-relief-card",
        title:"Hunger Relief Camp", 
        desc:"The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters",
        imgSrc:HungerImg,
        amount:"25"
    }
]

 
 export default cardDetails;