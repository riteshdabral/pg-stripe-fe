/**
 * Dashboard testing
 * 
 */

 import {shallow,mount} from 'enzyme'
 import Dashboard from './Dashboard'
 import {Elements} from '@stripe/react-stripe-js'
 import {loadStripe} from '@stripe/stripe-js'
 import {BrowserRouter as Router} from 'react-router-dom'

 let stripePromise = loadStripe(process.env.REACT_APP_STRIPE_TEST_API_KEY);
 let wrapper = mount(
    <Elements stripe={stripePromise}>
        <Router>
            <Dashboard />
        </Router>
    </Elements>
 )

 describe('DASHBOARD INTEGRATION TESTING', ()=>{

    it('Contains a jumbotron with data-test attribute', ()=>{
        expect(wrapper.find(`[data-test='dashboard-jumbotron']`)).toBeDefined();
    })

    it('Jumbotron 6 children', ()=>{
        expect(wrapper.find(`[data-test='dashboard-jumbotron']`).children()).toHaveLength(6);
    })

    it('Have stripe payment CardElement type component', ()=>{
        expect(wrapper.find(`CardInput`)).toBeDefined();
    })
 });