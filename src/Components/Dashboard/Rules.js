 import React from 'react'
 import {Card,Alert,Container} from 'react-bootstrap'
 
 function Rules() {
     return (
         <Container style={{padding:"10px"}}>

             <Card.Title>
                 <Alert variant="success">Rules for testing</Alert>
             </Card.Title>
             

             <Card.Body>
                <Alert variant="info">
                    Payments that don’t require authentication - 
                    4242 4242 4242 4242
                </Alert>
             </Card.Body>

             <Card.Body>
                <Alert variant="info">
                    Payments that require authentication - 
                    4000 0025 0000 3155
                </Alert>
             </Card.Body>

             <Card.Body>
                <Alert variant="info">
                    Card declines codes like insufficient_funds - 
                    4000 0000 0000 9995
                </Alert>
             </Card.Body>

         </Container>
     )
 }
 
 export default Rules
 