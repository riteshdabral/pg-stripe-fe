 import React from 'react'
 import {Card,Button} from 'react-bootstrap'

function AmountCard({title,desc,imgSrc,amount,setAmount}) {


    const handleSettingAmount = ()=>{
        setAmount(amount);
    }

    return (
        <Card style={{ width: '18rem',marginTop:"10px",marginBottom:"10px" }}>
            <Card.Img style={styles.customCardImg} variant="top" src={imgSrc} />
            <Card.Body>
                <Card.Title style={{fontWeight:"800"}}>{title}</Card.Title>
                <Card.Text style={{height:"100px",overflow:"auto"}}>{desc}</Card.Text>
                <Card.Text>Amount = {amount} INR</Card.Text>
                <Button variant="primary" onClick={handleSettingAmount}>Donate {amount} INR</Button>
            </Card.Body>
        </Card>
    )
}

 const styles = {
    customCardImg:{
        width: "200px",
        height: "200px",
        margin: "10px auto"
    }
 }

export default AmountCard
