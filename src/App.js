 
/* App CSS */ 
 import './App.css';
 import 'bootstrap/dist/css/bootstrap.min.css';

/* Components */
 import Homepage from './Components/Homepage/Homepage'
 import Dashboard from './Components/Dashboard/Dashboard'
 import {Elements} from '@stripe/react-stripe-js'
 import {loadStripe} from '@stripe/stripe-js'
 import {StripeTestApiKey} from './Config/index'

/* Additional Imports */
 import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'

function App() {

  const stripePromise = loadStripe(StripeTestApiKey);

  return (
    <Elements stripe={stripePromise}>
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={Homepage}/>
            <Route exact path="/dashboard" component={Dashboard}/>

            <Route exact path="*">
              <Redirect to="/" />
            </Route>
          </Switch>
        </Router>
      </div>
    </Elements>
  );
}

export default App;
