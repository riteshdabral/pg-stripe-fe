# [***`PG-stripe-FE`***](https://pg-stripe-fe.netlify.app) 

React based stripe payment gateway front end with only authentication with google and Facebook hosted on Netlify via CI/CD pipeline


## Requirements
  * NodeJS (curr ver - 12)
  * Browser
  * Postman (for testing APIs)
  * ReactJS
  * IDE or Text Editor
  * Stripe Developer Account
  * Google Developer Account
  * Facebook Developer Account

## Scripts
The project consists of well written and easily understandable test cases and descriptions.
#### `npm start`
     - This starts the project in development/production mode
#### `npm test`
     - This tests the DOM elements

## Important
    - Make sure to fill the `.env` file with your own keys

## Steps
1. run command `git clone https://gitlab.com/riteshdabral/pg-stripe-fe.git`
2. Within the root directory, run command `npm install`
3. Run the tests to make sure everything is working fine
4. If all test cases pass, run command `npm start` from root directory
5. Open your browser and test the application

## Live application
https://pg-stripe-fe.netlify.app
